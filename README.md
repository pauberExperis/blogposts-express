﻿# jQuery Demo With Express

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pages](https://img.shields.io/static/v1?logo=gitlab&message=view&label=pages&color=e24329)](https://noroff-accelerate.gitlab.io/javascript/jquery-skeleton)

> jQUery assignment with Blogpost API from https://jsonplaceholder.typicode.com/, paired with Express and put on Heroku

## Table of Contents

- [Install](#install)
- [Develop](#develop)
- [Build](#build)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)
- [Contribution](#Contribution)

## Install

```bash
npm install
```

## Develop

Start the Webpack development server.

```bash
npm start
```

## Build

Create a minified, production build of the project for publication.

```bash
npm run build
```

## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Experis © 2020 Noroff Accelerate AS
